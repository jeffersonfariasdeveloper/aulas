//
//  PrincipalViewController.m
//  Hello
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString * nome = @"Bruno";
    
    int idade = 27;
    
    CGPoint ponto = CGPointMake(30, 50);
    
    NSLog(@"Meu nome é %@ e idade %d",nome,idade);
    NSLog(@"O ponto ficou em %f e %f",ponto.x, ponto.y);
    

    //crio uma variavel que é um ponteiro para um label(basicamente é um label)
    UILabel * meuPrimeiroLabel;
    //aloco espaço necessário para armazenar um label na memoria
    meuPrimeiroLabel = [UILabel alloc];
    //inicializo as variaveis básicas desse objeto(construtor)
    meuPrimeiroLabel = [meuPrimeiroLabel init];
    
    //defino o texto desse label
    meuPrimeiroLabel.text = @"Olá Mundo!!!";
    //defino onde e com que tamanho esse label aparecerá
    meuPrimeiroLabel.frame = CGRectMake(10, 30, 200, 100);
    //para propriedades(variaveis) eu uso . em OBJC
    
    
    //uso um metodo da tela(self.view) para adicionar um elemento na tela
    [self.view addSubview:meuPrimeiroLabel];
    //para chamar métodos em OBJC eu uso []
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
