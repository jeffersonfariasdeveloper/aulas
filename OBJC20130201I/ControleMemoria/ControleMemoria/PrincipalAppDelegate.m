//
//  PrincipalAppDelegate.m
//  ControleMemoria
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalAppDelegate.h"

@implementation PrincipalAppDelegate

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    lista1 = [[NSArray alloc]initWithObjects:@"Legal",@"Maneiro",@"Show de bola!", nil];
    
    lista2 = [NSArray arrayWithObjects:@"Ruim",@"Zoado",@"BUG", nil];
    //deixo uma marca nesse objeto para mante-lo na memoria
    [lista2 retain];
        
    NSLog(@"Lista 1 %@",lista1);
    NSLog(@"Lista 2 %@",lista2);
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"Lista Volta 1 %@",lista1);
    NSLog(@"Lista Volta 2 %@",lista2);
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    //se eu aloquei ou retive objetos em memoria eu preciso libera-las
    [lista2 release];
    [lista1 release];
}

@end
