//
//  ImagemArmazenator.m
//  ListasEDicionarios
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "ImagemArmazenator.h"

@implementation ImagemArmazenator

-(id)init{
    self = [super init];
    if (self) {
        cache = [[NSMutableDictionary alloc]init];
    }
    return self;
}
-(UIImage*)getImageByURL:(NSString*)url{
    //verifico se essa url já foi salva no dicionario
    if ([cache objectForKey:url]==nil) {
        //nao existia no dicionario, baixarei e salvarei
        NSURL * urlFinal = [NSURL URLWithString:url];
        NSData * dados = [NSData dataWithContentsOfURL:urlFinal];
        //salvo os dados no dicionario
        [cache setObject:dados forKey:url];
        
        return [UIImage imageWithData:dados];
    }else{
        //já existia, entregarei os dados
        NSData * dados = [cache objectForKey:url];
        return [UIImage imageWithData:dados];
    }
}

@end
