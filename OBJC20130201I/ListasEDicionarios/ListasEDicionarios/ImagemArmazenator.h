//
//  ImagemArmazenator.h
//  ListasEDicionarios
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImagemArmazenator : NSObject{
    
     __strong NSMutableDictionary * cache;
    
}
-(UIImage*)getImageByURL:(NSString*)url;

//Em projetos ARC uso strong para variaveis que quero manter em memoria garantidamente
//com o weak eu não prendo o objeto na memoria, se o criador desse objeto libera-lo eu fico com p2=nil
@property(nonatomic,strong)NSString * p1;
@property(nonatomic,weak) NSString * p2;

//em projetos sem ARC uso retain no lugar do strong e assign no lugar do weak
@property(nonatomic,assign) NSString * p3;
@property(nonatomic,retain) NSString * p4;

//para tipos primitivos(com ou sem ARC) uso assign pois não preciso liberar nem reter na memoria
@property(nonatomic,assign)CGPoint p5;
@property(nonatomic,assign)int p6;

//posso usar o copy quando quero que ao receber um valor na propriedade eu trabalhe com uma copia
@property(nonatomic,copy)NSString * p7;

@end
