//
//  PrincipalViewController.h
//  Blocos
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrincipalViewController : UIViewController{
    
    //quando coloco um IBOutlet antes da variavel estou dizendo que ela sera criada no Interface Builder(IB) e não via codigo
    IBOutlet UIView * quadradinho;
}

-(IBAction)cliqueBotao:(id)sender;

@end
