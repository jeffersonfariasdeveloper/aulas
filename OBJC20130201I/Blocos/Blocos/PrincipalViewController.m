//
//  PrincipalViewController.m
//  Blocos
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

-(IBAction)cliqueBotao:(id)sender{
    NSLog(@"Clicou");
    
    [UIView animateWithDuration:0.5 animations:^{
        //aqui dentro coloco os comandos(função) que quero rodar
        quadradinho.frame = CGRectMake(rand() % 320, rand()%460, rand()%100, rand()%100);
        self.view.backgroundColor = [UIColor colorWithRed:(rand()%100)/100.0 green:(rand()%100)/100.0 blue:(rand()%100)/100.0 alpha:1];
        quadradinho.backgroundColor = [UIColor colorWithRed:(rand()%100)/100.0 green:(rand()%100)/100.0 blue:(rand()%100)/100.0 alpha:(rand()%100)/100.0];
        
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
