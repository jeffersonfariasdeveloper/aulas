//
//  PrincipalViewController.h
//  Thread
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrincipalViewController : UIViewController{
    IBOutlet UIImageView * imgFoto;
    IBOutlet UIActivityIndicatorView * loader;
    IBOutlet UISlider * sliderBobo;
}
-(IBAction)iniciarDownload:(id)sender;

@end
