//
//  PrincipalViewController.m
//  Thread
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

-(IBAction)iniciarDownload:(id)sender{
    //ligo o spinner
    [loader startAnimating];
    //o processo de download trava a thread principal, entao disparo em uma thread secundaria para que mainThread possa manter a tela funcionando
    [NSThread detachNewThreadSelector:@selector(downloadDemorado) toTarget:self withObject:nil];
}
-(void)downloadDemorado{
    //não posso atualizar elementos da tela numa thread secundaria, para isso preciso solicitar para mainThread atualizar
    [self performSelectorOnMainThread:@selector(atualizaInterface) withObject:nil waitUntilDone:FALSE];
    
    NSURL * url =[NSURL URLWithString:@"http://collider.com/wp-content/uploads/the-dark-knight-rises-anne-hathaway-catwoman-image1.jpg"];
    //baixo o conteudo da url acima
    NSData * dados = [NSData dataWithContentsOfURL:url];
    //converto para imagem
    UIImage * img = [UIImage imageWithData:dados];
    //apresento na tela
    imgFoto.image = img;
    
    //desligo o spinner
    [loader stopAnimating];
}
-(void)atualizaInterface{
    sliderBobo.value = 0;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
