//
//  PrincipalViewController.m
//  Excecoes
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	@try {
        NSLog(@"Aqui coloco os codigos que desejo fazer");
        //[[NSBundle mainBundle]loadNibNamed:@"fsdfs" owner:self options:nil];
        int x=5;
        x = 8/(5-x);
        
    }
    @catch (NSException *exception) {
        NSLog(@"Se der algum erro no Try ele entra aqui");
    }
    @finally {
        NSLog(@"Independente de ter dado erro ou nao entra aqui");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
