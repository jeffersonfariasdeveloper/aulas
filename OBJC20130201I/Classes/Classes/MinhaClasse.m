//
//  MinhaClasse.m
//  Classes
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "MinhaClasse.h"

@implementation MinhaClasse
//@synthesize x2,nome2;

#pragma mark metodos de instancia
-(void)metodoSimples{
    
}
-(NSString*)metodoComplexo:(float)tamanho{
    return @"";
}
-(BOOL)salvarUsuario:(NSString*)nomeUsuario senha:(NSString*)psw statusInicial:(TipoStatus)status{
    //os valores vieram pelas variaveis nomeUsuario, psw e status
    NSLog(@"Nome: %@ senha:%@ status:%d",nomeUsuario,psw,status );
    return  true;
}
#pragma mark metodos de classe
+(void)metodoDeClasse{
    
}
#pragma mark metodos de memoria
-(id)init{
    self = [super init];
    if (self) {
        //aqui coloco os comandos de inicializacao do meu objeto
        x= 5;
        nome =@"Nome Padrao";
    }
    return self;
}
-(id)initWithInitialName:(NSString*)n value:(int)v{
    self = [super init];
    if (self!=nil) {
        nome = n;
        x = v;
    }
    return self;
}
-(void)dealloc{

    //aqui coloco comandos que liberam recursos do objeto
}


@end
