//
//  MinhaClasse.h
//  Classes
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ATIVO_MODE 0;
#define INATIVO_MODE 1;
#define BANIDO_MODE 2;
#define PATH_SERVIDOR @"http://brunoeiti.com";

typedef enum TipoStatus{
    ATIVO,
    INATIVO,
    BANIDO
}TipoStatus;

@interface MinhaClasse : NSObject {
    //dentro do {} poss colocar os atributos, em OBJC atributos sao privados
    int x;
    NSString * nome;
}
//fora do {} posso usar propriedades que são variaveis publicas
@property int x2;
@property NSString * nome2;

-(void)metodoSimples;
//public void metodoSimples()

//metodo que retorna um Texto e recebe um parametro float chamado tamanho
-(NSString*)metodoComplexo:(float)tamanho;
//public String metodoComplexo(float tamanho)

-(BOOL)salvarUsuario:(NSString*)nomeUsuario senha:(NSString*)psw statusInicial:(TipoStatus)status;
//public salvarUsuarioSenhaStatusInicial(String nome, String psw, int status)

//posso declarar metodos construtores
-(id)init;
-(id)initWithInitialName:(NSString*)n value:(int)v;

//metodo chamado quando esse objeto sera destruido
-(void)dealloc;

//metodo de classe(+) static
+(void)metodoDeClasse;
//public static void metodoDeClasse()

@end
