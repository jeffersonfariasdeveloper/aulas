//
//  PrincipalViewController.m
//  Classes
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //aloco e inicializo um objeto
    meuObjeto = [[MinhaClasse alloc]initWithInitialName:@"Bruno" value:4];
    //posso invocar os métodos que criei usando []
    [meuObjeto metodoSimples];
    
    NSString * texto = [meuObjeto metodoComplexo:5.5];
    
    [meuObjeto salvarUsuario:@"bruno" senha:texto statusInicial:ATIVO];
    
    //para metodos de classe continuo usando o [] mas coloco o nome da classe direto, nao do objeto
    [MinhaClasse metodoDeClasse];
    
    //para propriedade uso  .
    meuObjeto.nome2 = @"RRRR";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
