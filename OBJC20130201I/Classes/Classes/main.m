//
//  main.m
//  Classes
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PrincipalAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PrincipalAppDelegate class]));
    }
}
