//
//  PrincipalViewController.m
//  Excecoes2
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"

@interface PrincipalViewController ()

@end

@implementation PrincipalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	NSString * texto = @"dsadadas";
    
    //inicio minha variavel de erro como nula
    NSError * erro = nil;
    [texto writeToFile:@"fsdsd" atomically:true encoding:NSUTF8StringEncoding error:&erro];
    //se apos o metodo ela deixar de ser nula significa que houve um erro no processo
    if (erro!=nil) {
        NSLog(@"PAM!! %@",erro.localizedDescription);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
