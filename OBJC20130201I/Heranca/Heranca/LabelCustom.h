//
//  LabelCustom.h
//  Heranca
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelCustom : UILabel

-(void)trocarVogais;

@end
