//
//  PrincipalViewController.m
//  Heranca
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "PrincipalViewController.h"
#import "LabelCustom.h"

@interface PrincipalViewController ()
@end

@implementation PrincipalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    LabelCustom * lbl = [[LabelCustom alloc]initWithFrame:CGRectMake(10, 40, 150, 70)];
    
    lbl.text = @"Cores bonitas";
    [self.view addSubview:lbl];
    
    [lbl trocarVogais];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
