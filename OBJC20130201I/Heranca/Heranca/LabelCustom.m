//
//  LabelCustom.m
//  Heranca
//
//  Created by Bruno Hassuna on 2/2/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import "LabelCustom.h"

@implementation LabelCustom

//posso sobreescrever um método que foi definido numa classe pai
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor redColor];
        self.textColor = [UIColor blueColor];
    }
    return self;
}

-(void)trocarVogais{
    self.text = [self.text stringByReplacingOccurrencesOfString:@"a" withString:@"*"];
    self.text = [self.text stringByReplacingOccurrencesOfString:@"e" withString:@"*"];
    self.text = [self.text stringByReplacingOccurrencesOfString:@"i" withString:@"*"];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
