//
//  PrincipalViewController.h
//  ListasDelegate
//
//  Created by Bruno Hassuna on 2/3/13.
//  Copyright (c) 2013 Bruno Hassuna. All rights reserved.
//

#import <UIKit/UIKit.h>

//quando coloco <UITextFieldDelegate> estou me comprometendo a implementar os metodos(avisos) do textField
@interface PrincipalViewController : UIViewController <UITextFieldDelegate>{
    
    IBOutlet UITextField * txtNome, * txtEmail, *txtTelefone;
    
    NSMutableArray * lista;
}
-(IBAction)salvar:(id)sender;
-(IBAction)ler:(id)sender;

@end
